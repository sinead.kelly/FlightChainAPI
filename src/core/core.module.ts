import { Inject, Module } from '@nestjs/common';
import { Appconfig } from '../common/config/appconfig';
import { Log } from '../common/utils/logging/log.service';
import { EnvConfig } from '../common/config/env';
import { Json } from '../common/utils/json';
import {ChainModule} from "./chain-interface/chain.module";
import {HlfClient} from "./chain-interface/hlfclient";
import {HlfErrors, HlfInfo} from "./chain-interface/logging.enum";
import {User} from "fabric-client";
import {HlfConfig} from "./chain-interface/hlfconfig";

@Module({
    imports: [
        ChainModule,
    ]
})
export class CoreModule {

    /**
     * Creates an instance of ApplicationModule.
     * @param {HlfClient} hlfClient
     * @param caClient
     * @param {QueueListenerService} queueListenerService
     * @param webSocketService
     * @memberof ApplicationModule
     */
    constructor(private hlfClient: HlfClient,
                private hlfConfig: HlfConfig) {

        // init hlf client and hlf ca client
        // assign admin user
        this.hlfClient.init()
            .then(result => {


                /*
                if (!EnvConfig.BYPASS_QUEUE) {
                    Log.awssqs.info(`Starting Queue Listener...`);
                    return this.queueListenerService.listen((message, done) => {
                        Log.awssqs.debug(`Handling new queue item from ${EnvConfig.AWS_QUEUE_NAME}:`, message);

                        const {chainMethod, payload, userId} = <MessageBody>Json.deserializeJson(message.Body);
                        const pusherChannel = userId.replace(/[!|@#$%^&*]/g, '');

                        this.hlfClient.invoke(chainMethod, payload)
                            .then(() => {
                                Log.awssqs.info('HLF Transaction successful, pushing result to frontend...');
                                // notify frontend of succesful transaction
                                this.webSocketService.triggerSuccess(pusherChannel, chainMethod, payload);
                                done();
                            })
                            .catch(error => {
                                Log.awssqs.error('HLF Transaction failed:', error);
                                // notify frontend of failed transaction
                                this.webSocketService.triggerError(pusherChannel, chainMethod, {success: false});
                                done(error);
                            });

                    });
                }*/

                return Promise.resolve();
            })
            .then(() => {
                return this.getUserFromStore(EnvConfig.IDENTITY);
            })
            .catch(err => {
                Log.awssqs.error(HlfErrors.ERROR_STARTING_HLF, err.message);
            });
    }


    getUserFromStore(userId: string, checkPersistence = true): Promise<User> {
        return (this.hlfConfig.client.getUserContext(userId, checkPersistence) as Promise<User>)
            .then(userFromStore => {
                if (userFromStore && userFromStore.isEnrolled()) {
                    Log.hlf.info(HlfInfo.LOAD_USER_SUCCESS, userId);
                    return userFromStore;
                } else {
                    Log.hlf.error(HlfErrors.LOAD_USER_ERROR, userId);
                    process.exit(1);
                }
            });
    }

}