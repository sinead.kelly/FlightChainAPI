import { HlfInfo } from './logging.enum';
import { Injectable } from '@nestjs/common';
import { ChainService } from './chain.service';
import { HlfConfig } from './hlfconfig';
import { IKeyValueStore, ProposalResponseObject, TransactionRequest } from 'fabric-client';
import FabricClient = require('fabric-client');
import { Appconfig } from '../../common/config/appconfig';
// import { ChainMethod } from '../../chainmethods.enum';
import { Log } from '../../common/utils/logging/log.service';
import {FlightChainMethod} from "../../flight-chain2/flight-chain2.service";
import * as Client from "fabric-client";
import * as fs from "fs";

@Injectable()
export class HlfClient extends ChainService {

    static config = {
        "peers" : [
            {
                "endpoint" : "grpcs://peer1st-sitaeu.fabric12:7051",
                "pemFile" : "/tmp/crypto/peerOrganizations/sitaeu.fabric12/peers/peer1st-sitaeu.fabric12/msp/tlscacerts/tlsca.sitaeu.fabric12-cert.pem"
            },
            {
                "endpoint" : "grpcs://peer2nd-sitaeu.fabric12:7051",
                "pemFile" : "/tmp/crypto/peerOrganizations/sitaeu.fabric12/peers/peer1st-sitaeu.fabric12/msp/tlscacerts/tlsca.sitaeu.fabric12-cert.pem"
            }
        ],
        "orderers" : [
            {
                "endpoint" : "grpcs://orderer1st-sita.fabric12:7050",
                "pemFile" : "/tmp/crypto/ordererOrganizations/fabric12/orderers/orderer1st-sita.fabric12/msp/tlscacerts/tlsca.fabric12-cert.pem"
            },
            {
                "endpoint" : "grpcs://orderer2nd-sita.fabric12:7050",
                "pemFile" : "/tmp/crypto/ordererOrganizations/fabric12/orderers/orderer2nd-sita.fabric12/msp/tlscacerts/tlsca.fabric12-cert.pem"
            }
        ],
        "eventHub" : {
            "endpoint" : "grpcs://peer1st-sitaeu.fabric12:7053",
            "pemFile" : "/tmp/crypto/peerOrganizations/sitaeu.fabric12/peers/peer1st-sitaeu.fabric12/msp/tlscacerts/tlsca.sitaeu.fabric12-cert.pem"
        }
    };

    constructor(public hlfConfig: HlfConfig) {
        super(hlfConfig);
    }

    /**
     * init
     * @returns {Promise<any>}
     * @memberof ChainService
     */
    init(): Promise<any> {

        this.hlfConfig.options = Appconfig.hlf;
        this.hlfConfig.client = new FabricClient();

        Log.hlf.info('HlfClient.init()', this.hlfConfig);

        return FabricClient
            .newDefaultKeyValueStore({
                path: this.hlfConfig.options.walletPath
            })
            .then((wallet: IKeyValueStore) => {
                console.log(wallet);
                // assign the store to the fabric client
                this.hlfConfig.client.setStateStore(wallet);
                let cryptoSuite = FabricClient.newCryptoSuite();
                // use the same location for the state store (where the users' certificate are kept)
                // and the crypto store (where the users' keys are kept)
                let cryptoStore = FabricClient.newCryptoKeyStore({path: this.hlfConfig.options.walletPath});
                cryptoSuite.setCryptoKeyStore(cryptoStore);
                this.hlfConfig.client.setCryptoSuite(cryptoSuite);
                this.hlfConfig.channel = this.hlfConfig.client.newChannel(this.hlfConfig.options.channelId);


                HlfClient.config.peers.forEach(peer => {

                    var connectionOptions:Client.ConnectionOptions = {};
                    const data = fs.readFileSync(peer.pemFile);
                    connectionOptions.pem = Buffer.from(data).toString();
                    Log.hlf.debug(`Connecting to peer ${peer.endpoint}, ${connectionOptions}`);
                    console.log('connectionOptions', connectionOptions);
                    const peerObj = this.hlfConfig.client.newPeer(peer.endpoint, connectionOptions) ;//, this.connectionOptions);
                        this.hlfConfig.channel.addPeer(peerObj);
                        this.hlfConfig.targets.push(peerObj);
                    })


                var connectionOptions:Client.ConnectionOptions = {};
                const data = fs.readFileSync(HlfClient.config.orderers[0].pemFile);
                connectionOptions.pem = Buffer.from(data).toString();
                Log.hlf.debug(`Connecting to orderer ${HlfClient.config.orderers[0].endpoint}, ${connectionOptions}`);
                this.hlfConfig.channel.addOrderer(this.hlfConfig.client.newOrderer(HlfClient.config.orderers[0].endpoint, connectionOptions));


                // this.hlfConfig.options.peerUrls.forEach(peer => {
                //     Log.hlf.debug(`Connecting to peer ${peer}`);
                //     const peerObj = this.hlfConfig.client.newPeer(peer) ;//, this.connectionOptions);
                //     this.hlfConfig.channel.addPeer(peerObj);
                //     this.hlfConfig.targets.push(peerObj);
                // });
                // this.hlfConfig.channel.addOrderer(this.hlfConfig.client.newOrderer(this.hlfConfig.options.ordererUrl));
                Log.hlf.info(HlfInfo.INIT_SUCCESS);
            });
    }

    /**
     * Query hlf
     *
     * @param {FlightChainMethod} chainMethod
     * @param {string[]} params
     * @param transientMap
     * @returns {Promise<any>}
     * @memberof HlfClient
     */
    query(chainMethod: FlightChainMethod, params: string[], transientMap?: Object): Promise<any> {
        Log.hlf.info(HlfInfo.MAKE_QUERY, chainMethod, params);
        return this.newQuery(chainMethod, params, this.hlfConfig.options.chaincodeId, transientMap)
            .then((queryResponses: Buffer[]) => {
                return Promise.resolve(this.getQueryResponse(queryResponses));
            });
    }
    /**
     * Query hlf
     *
     * @param {FlightChainMethod} chainMethod
     * @param {string[]} params
     * @param transientMap
     * @returns {Promise<any>}
     * @memberof HlfClient
     */
    queryTransaction(transactionId:string ): Promise<any> {
        Log.hlf.info(HlfInfo.QUERY_TRANSACTIONID, transactionId);
        return this.newTransactionQuery(transactionId);
    }

    /**
     * invoke
     *
     * @param {FlightChainMethod} chainMethod
     * @param { string[]} params
     * @param transientMap
     * @returns
     * @memberof ChainService
     */
    invoke(chainMethod: FlightChainMethod, params: string[], transientMap?: Object): Promise<any> {
        Log.hlf.info(chainMethod, params);
        return this.sendTransactionProposal(chainMethod, params, this.hlfConfig.options.chaincodeId, transientMap)
            .then((result: { txHash: string; buffer: ProposalResponseObject }) => {
                // Log.hlf.debug(JSON.stringify(result.buffer));
                Log.hlf.info(HlfInfo.CHECK_TRANSACTION_PROPOSAL);
                if (this.isProposalGood(result.buffer)) {

                    this.logSuccessfulProposalResponse(result.buffer);

                    let request: TransactionRequest = {
                        proposalResponses: result.buffer[0],
                        proposal: result.buffer[1]
                    };
                    Log.hlf.info(HlfInfo.REGISTERING_TRANSACTION_EVENT);

                    let sendPromise = this.hlfConfig.channel.sendTransaction(request);
                    let txPromise = this.registerTxEvent(result.txHash);

                    return Promise.all([sendPromise, txPromise]);
                } else {

                    let message = result.buffer[0][0].response.message;

                    if (message.indexOf(' transaction returned with failure: ') !== -1) {
                        message = message.split(' transaction returned with failure: ')[1];

                        try {
                            message = JSON.parse(message);
                        } catch (e) {
                            Log.hlf.error(e);
                        }
                    }
                    return Promise.reject(message);
                }
            })
            .then((results) => {
                if (!results || (results && results[0] && results[0].status !== 'SUCCESS')) {
                    Log.hlf.error('Failed to order the transaction. Error code: ' + results[0].status);
                }

                if (!results || (results && results[1] && results[1].event_status !== 'VALID')) {
                    Log.hlf.error('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
                }
            });
    }
}
