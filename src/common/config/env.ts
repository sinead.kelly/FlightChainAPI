import 'dotenv/config';
import * as path from "path";
import {HlfConfig} from "../../core/chain-interface/hlfconfig";
import {Log} from "../utils/logging/log.service";

export interface ProcessEnv {
    [key: string]: string | undefined;
}

/**
 * node EnvConfig variables,
 * copy .env.example file, rename to .env
 *
 * @export
 * @class EnvConfig
 */
export class EnvConfig {


    static initialise () {
        // list env keys in console
        Log.config.debug('Initialising the environment variables.');

        Log.config.debug(`EnvConfig.USE_TLS is set to ${EnvConfig.USE_TLS} - verifing the endpoints do/do not have TLS`);
        if (EnvConfig.USE_TLS) {

            for (var i=0; i<EnvConfig.PEERS.length; i++) {
                EnvConfig.PEERS[i] = EnvConfig.PEERS[i].replace('grpc://', 'grpcs://');
            }
            EnvConfig.ORDERER = EnvConfig.ORDERER.replace('grpc://', 'grpcs://');
            EnvConfig.EVENTHUB = EnvConfig.EVENTHUB.replace('grpc://', 'grpcs://');
            EnvConfig.CA_ENDPOINT = EnvConfig.CA_ENDPOINT.replace('http://', 'https://');
        } else {
            for (var i=0; i<EnvConfig.PEERS.length; i++) {
                EnvConfig.PEERS[i] = EnvConfig.PEERS[i].replace('grpcs://', 'grpc://');
            }
            EnvConfig.EVENTHUB = EnvConfig.EVENTHUB.replace('grpcs://', 'grpc://');
            EnvConfig.ORDERER = EnvConfig.ORDERER.replace('grpcs://', 'grpc://');
            EnvConfig.CA_ENDPOINT = EnvConfig.CA_ENDPOINT.replace('https://', 'http://');

        }
        for (let propName of Object.keys(EnvConfig)) {
            Log.config.debug(`${propName}:  ${EnvConfig[propName]}`);
        }
    }
    // NODE
    public static LISTENING_PORT = process.env['LISTENING_PORT'] || 3000;
    public static NODE_ENV = process.env['NODE_ENV'] || 'LOCAL';


    // FABRIC
    public static USE_TLS = 'true' === process.env['USE_TLS'];
    public static CA_ENDPOINT = process.env['CA_HOST'] || 'https://localhost:7054';
    public static IDENTITY = process.env['IDENTITY'];
    public static HFC_STORE_PATH = process.env['HFC_STORE_PATH'] || path.join('./', 'bootstrap/hfc-key-store');
    public static CHANNEL = process.env['CHANNEL'] || 'channel-flight-chain';


    // Organisational MSP Id
    public static MSPID: string = process.env['MSPID'] || 'SITAMSP';

    // can contain comma separated list of peers to connect to
    public static PEERS = process.env['PEERS'] ? process.env['PEERS'].split(',') :['grpcs://localhost:7051', 'grpcs://localhost:8051'];

    public static ORDERER = process.env['ORDERER'] || 'grpcs://localhost:7050';
    public static EVENTHUB = process.env['EVENTHUB'] || 'grpcs://localhost:7053'

    // True if this app is in demo mode, and not connected to a fabric network.
    public static IS_DEMO_MODE = process.env['IS_DEMO_MODE'] || false;

}