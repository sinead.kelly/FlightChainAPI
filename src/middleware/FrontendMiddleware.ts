import {ExpressMiddleware} from "@nestjs/common/interfaces/middlewares/express-middleware.interface";
import {NestMiddleware} from "@nestjs/common";
import {Middleware} from "@nestjs/common/utils/decorators/component.decorator";
import * as path from "path";
import {DOCS_URL} from "../main";
import {Request} from 'express'

export const FLIGHTCHAIN_ROUTE_PREFIX = 'flightChain';
export const HEALTH_ROUTE_PREFIX = 'health';

const allowedExt = [
    '.js',
    '.ico',
    '.css',
    '.png',
    '.jpg',
    '.woff2',
    '.woff',
    '.ttf',
    '.svg',
];

const resolvePath = (file: string) => path.resolve(`./dist/flight-chain-ui/${file}`);

@Middleware()
export class FrontendMiddleware implements NestMiddleware {
    resolve(...args: any[]): ExpressMiddleware {
        return (req: Request, res, next) => {
            const  url  = req.baseUrl;
            if (url.indexOf(FLIGHTCHAIN_ROUTE_PREFIX) === 1) {
                // it starts with /api --> continue with execution
                next();
            } else if (url.indexOf(HEALTH_ROUTE_PREFIX) === 1) {
                    // it starts with /api --> continue with execution
                    next();
            } else if (url.indexOf(DOCS_URL) === 1) {
                    // it starts with /docs --> continue with execution
                    next();
            } else if (allowedExt.filter(ext => url.indexOf(ext) > 0).length > 0) {
                // it has a file extension --> resolve the file
                res.sendFile(resolvePath(url));
            } else {
                // in all other cases, redirect to the index.html!
                res.sendFile(resolvePath('index.html'));
            }
        };
    }
}
